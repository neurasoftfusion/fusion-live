nssm install fusionEVOLUTIONWebAPI FusionWebAPIServer.exe
nssm set fusionEVOLUTIONWebAPI AppDirectory %~dp0
nssm set fusionEVOLUTIONWebAPI DisplayName fusionEVOLUTION Web API
nssm set fusionEVOLUTIONWebAPI Description This is the Neurasoft fusionEVOLUTION Web API.
nssm set fusionEVOLUTIONWebAPI AppRestartDelay 5000
