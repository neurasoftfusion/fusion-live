select 
		case lines.iModule
			when 0 then 'IC'
			when 1 then 'GL'
			else ''
		end ModuleType,
		case lines.iModule
			when 0 then StkItem.Code
			when 1 then GeneralLedgerAccount.Master_Sub_Account
			else ''
		end ItemCode,
		lines.*,
		header.*,
		IsNull(Currency.CurrencyCode, CompanyDefaults.cHomeCurrency) CurrencyCode,
		IsNull(Currency.cCurrencySymbol, CompanyDefaults.cHomeCurrency) CurrencySymbol,
		CompanyDefaults.cHomeCurrency HomeCurrencySymbol,
		(select top 1 cAgentName 
			from _rtblIncidentLog 
				left join _rtblAgents
					on _rtblAgents.idAgents = _rtblIncidentLog.iAgentID
			where iIncidentActionID = 9 
				and iIncidentID = header.iPOIncidentID 
			order by idIncidentLog desc) approvedByAgentName
	from _btblInvoiceLines lines
		left join InvNum header
			on header.AutoIndex = lines.iInvoiceID
		left join StkItem
			on StkItem.StockLink = lines.iStockCodeID
		left join Accounts GeneralLedgerAccount
			on GeneralLedgerAccount.AccountLink = lines.iLedgerAccountID
		left join Currency 
			on CurrencyLink = header.ForeignCurrencyID
		left join Entities CompanyDefaults
			on CompanyDefaults.idEntities = 1
	where header.AutoIndex = {primaryKeyValue}
