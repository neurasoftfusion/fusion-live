select 
		postalCountry.cCountryName POCountryName,
		postalState.cStateDescription POStateName,
		'logo.jpg' logoFileName,
		Entities.*
	from Entities 
		left join _rtblCountry postalCountry
			on postalCountry.idCountry = Entities.POCountry
		left join _btblState postalState
			on postalState.idState = Entities.POState
	where idEntities = 1

