select convert(varchar(4), year(header.OrderDate)) orderDateYear,
	   replace(str(month(header.OrderDate), 2),' ','0') orderDateMonth
	from InvNum header
	where header.AutoIndex = {primaryKeyValue}
	