// Generated by CoffeeScript 1.11.1
(function() {
  'use strict';

  /*
   
   Neurasoft Consulting CONFIDENTIAL
   __________________
   
    Copyright (C) Neurasoft Consulting cc
    All Rights Reserved.
   
   NOTICE:  All information contained herein is, and remains
   the property of Neurasoft Consulting cc and its suppliers,
   if any.  The intellectual and technical concepts contained
   herein are proprietary to Neurasoft Consulting cc
   and its suppliers and are protected by trade secret or copyright law.
   Dissemination of this information or reproduction of this material
   is strictly forbidden unless prior written permission is obtained
   from Neurasoft Consulting cc.
   */
  var PdfViewerService;

  PdfViewerService = function($window) {
    var OpenPdf, service;
    OpenPdf = function(uri) {
      return $window.open("/lib/pdf-viewer/web/viewer.html?file=/" + uri, '_blank');
    };
    service = {
      OpenPdf: OpenPdf
    };
    return service;
  };

  PdfViewerService.$inject = ['$window'];

  angular.module('fusion-core').factory('PdfViewerService', PdfViewerService);

}).call(this);
