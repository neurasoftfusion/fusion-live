// Generated by CoffeeScript 1.11.1
(function() {
  'use strict';

  /*
  
   Neurasoft Consulting CONFIDENTIAL
   __________________
  
    Copyright (C) Neurasoft Consulting cc
    All Rights Reserved.
  
   NOTICE:  All information contained herein is, and remains
   the property of Neurasoft Consulting cc and its suppliers,
   if any.  The intellectual and technical concepts contained
   herein are proprietary to Neurasoft Consulting cc
   and its suppliers and are protected by trade secret or copyright law.
   Dissemination of this information or reproduction of this material
   is strictly forbidden unless prior written permission is obtained
   from Neurasoft Consulting cc.
   */
  var LoginService,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  LoginService = (function() {
    function LoginService(LoginWebAPI, LoginEventService, CurrentUserService, AuthenticationTokenService, TokenPayloadService, CompanyDefaultsService, NavigationService, MainMenuEventService) {
      this.LoginWebAPI = LoginWebAPI;
      this.LoginEventService = LoginEventService;
      this.CurrentUserService = CurrentUserService;
      this.AuthenticationTokenService = AuthenticationTokenService;
      this.TokenPayloadService = TokenPayloadService;
      this.CompanyDefaultsService = CompanyDefaultsService;
      this.NavigationService = NavigationService;
      this.MainMenuEventService = MainMenuEventService;
      this.login = bind(this.login, this);
    }

    LoginService.prototype.login = function(credentials) {
      return this.validateUserCredentials(credentials);
    };

    LoginService.prototype.validateUserCredentials = function(credentials) {
      return this.LoginWebAPI.login(credentials).then((function(_this) {
        return function(response) {
          return _this.processAgentLoginResponse(response.fusion, credentials);
        };
      })(this));
    };

    LoginService.prototype.processAgentLoginResponse = function(fusionData, credentials) {
      if (this.userCredentialsValid(fusionData)) {
        return this.successfulLogin(fusionData, credentials);
      } else {
        return this.failedLogin();
      }
    };

    LoginService.prototype.userCredentialsValid = function(fusionData) {
      var ref, ref1;
      return ((ref = fusionData != null ? (ref1 = fusionData.data) != null ? ref1.token : void 0 : void 0) != null ? ref : '') !== '';
    };

    LoginService.prototype.successfulLogin = function(fusionData, credentials) {
      var token;
      token = this.extractToken(fusionData);
      this.decodePayload(fusionData);
      this.setCurrentUser(fusionData.tokenPayload, credentials);
      this.CompanyDefaultsService.setDefaults(fusionData.data.defaults);
      this.AuthenticationTokenService.addTokenToRestangularDefaultHeaders(token);
      this.storeTokenLocally(token);
      this.storePayloadLocally(fusionData.tokenPayload);
      this.extractBranchData(fusionData);
      if (this.CurrentUserService.isBranchAccounting) {
        return this.NavigationService.goSelectBranch();
      } else {
        this.MainMenuEventService.broadcastShowMainMenu();
        console.log("@NavigationService.redirectAfterLogin.routeName", this.NavigationService.redirectAfterLogin.routeName);
        if (!_.isUndefined(this.NavigationService.redirectAfterLogin.routeName) && this.NavigationService.redirectAfterLogin.routeName !== '') {
          return this.NavigationService.doRedirectAfterLogin();
        } else {
          return this.NavigationService.goHome();
        }
      }
    };

    LoginService.prototype.extractToken = function(fusionData) {
      var ref, ref1;
      return (ref = fusionData != null ? (ref1 = fusionData.data) != null ? ref1.token : void 0 : void 0) != null ? ref : '';
    };

    LoginService.prototype.decodePayload = function(fusionData) {
      var ref, ref1, token;
      token = (ref = fusionData != null ? (ref1 = fusionData.data) != null ? ref1.token : void 0 : void 0) != null ? ref : '';
      return fusionData.tokenPayload = angular.fromJson(atob(token.replace('Bearer ', '').split('.')[1]));
    };

    LoginService.prototype.storeTokenLocally = function(token) {
      return this.AuthenticationTokenService.storeAuthenticationToken(token);
    };

    LoginService.prototype.storePayloadLocally = function(tokenPayload) {
      return this.TokenPayloadService.storeTokenPayload(tokenPayload);
    };

    LoginService.prototype.setCurrentUser = function(tokenPayload, credentials) {
      if (!tokenPayload) {
        return;
      }
      credentials.userId = parseInt(tokenPayload['user-id']);
      credentials.userRoles = tokenPayload['user-role'];
      return this.CurrentUserService.setCurrentUser(credentials);
    };

    LoginService.prototype.extractBranchData = function(fusionData) {
      var ref, ref1;
      return this.CurrentUserService.setBranchList((ref = fusionData != null ? (ref1 = fusionData.data) != null ? ref1.branchList : void 0 : void 0) != null ? ref : []);
    };

    LoginService.prototype.failedLogin = function() {
      return this.LoginEventService.broadcastFailedLogin();
    };

    return LoginService;

  })();

  LoginService.$inject = ['LoginWebAPI', 'LoginEventService', 'CurrentUserService', 'AuthenticationTokenService', 'TokenPayloadService', 'CompanyDefaultsService', 'NavigationService', 'MainMenuEventService'];

  angular.module('fusion-core').service('LoginService', LoginService);

}).call(this);
