// Generated by CoffeeScript 1.11.1
(function() {
  'use strict';

  /*
   
   Neurasoft Consulting CONFIDENTIAL
   __________________
   
    Copyright (C) Neurasoft Consulting cc
    All Rights Reserved.
   
   NOTICE:  All information contained herein is, and remains
   the property of Neurasoft Consulting cc and its suppliers,
   if any.  The intellectual and technical concepts contained
   herein are proprietary to Neurasoft Consulting cc
   and its suppliers and are protected by trade secret or copyright law.
   Dissemination of this information or reproduction of this material
   is strictly forbidden unless prior written permission is obtained
   from Neurasoft Consulting cc.
   */
  var TaxTypeWebApi;

  TaxTypeWebApi = function(WebAPIService) {
    var getTaxType, getTaxTypeAccountList, service;
    getTaxTypeAccountList = function() {
      return WebAPIService.doGETRequest('tax-types/tax-type-list');
    };
    getTaxType = function(taxTypeId) {
      return WebAPIService.doGETRequest("tax-types/tax-type/" + taxTypeId);
    };
    service = {
      getTaxTypeAccountList: getTaxTypeAccountList,
      getTaxType: getTaxType
    };
    return service;
  };

  TaxTypeWebApi.$inject = ['WebAPIService'];

  angular.module('fusion-core').factory('TaxTypeWebApi', TaxTypeWebApi);

}).call(this);
