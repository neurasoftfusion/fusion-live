// Generated by CoffeeScript 1.11.1
(function() {
  'use strict';

  /*
  
   Neurasoft Consulting CONFIDENTIAL
   __________________
  
    Copyright (C) Neurasoft Consulting cc
    All Rights Reserved.
  
   NOTICE:  All information contained herein is, and remains
   the property of Neurasoft Consulting cc and its suppliers,
   if any.  The intellectual and technical concepts contained
   herein are proprietary to Neurasoft Consulting cc
   and its suppliers and are protected by trade secret or copyright law.
   Dissemination of this information or reproduction of this material
   is strictly forbidden unless prior written permission is obtained
   from Neurasoft Consulting cc.
   */
  var MessageDialog, MessageDialogController;

  MessageDialog = function($modal) {
    var ShowErrorMessage, ShowMessage, model, service;
    model = this;
    model.dialogInstance = null;
    model.callBack = null;
    ShowMessage = function(title, message, callBack, messageType) {
      if (messageType == null) {
        messageType = 'info';
      }
      model.callBack = callBack;
      model.dialogInstance = $modal.open({
        templateUrl: FusionWrapClientVersion('app/core/prompts/message-dialog.html'),
        resolve: {
          Title: function() {
            return title;
          },
          Message: function() {
            return message;
          },
          MessageType: function() {
            return messageType;
          }
        },
        size: 'sm',
        backdrop: true,
        keyboard: true,
        controllerAs: 'modalCtrl',
        controller: 'MessageDialogController'
      });
      return model.dialogInstance.result.then(function() {
        if (model.callBack) {
          return model.callBack();
        }
      });
    };
    ShowErrorMessage = function(title, message, callBack) {
      return ShowMessage(title, message, callBack, 'error');
    };
    service = {
      ShowMessage: ShowMessage,
      ShowErrorMessage: ShowErrorMessage
    };
    return service;
  };

  MessageDialog.$inject = ['$modal'];

  MessageDialogController = function($modalInstance, Title, Message, MessageType) {
    var model;
    model = this;
    model.Title = Title;
    model.Message = Message;
    model.errorMessage = MessageType === 'error';
    model.ok = function() {
      return $modalInstance.close();
    };
    return null;
  };

  MessageDialogController.$inject = ['$modalInstance', 'Title', 'Message', 'MessageType'];

  angular.module('fusion-prompts').factory('MessageDialog', MessageDialog).controller('MessageDialogController', MessageDialogController);

}).call(this);
