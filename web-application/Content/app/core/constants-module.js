// Generated by CoffeeScript 1.11.1
(function() {
  'use strict';

  /*
  
   Neurasoft Consulting CONFIDENTIAL
   __________________
  
    Copyright (C) Neurasoft Consulting cc
    All Rights Reserved.
  
   NOTICE:  All information contained herein is, and remains
   the property of Neurasoft Consulting cc and its suppliers,
   if any.  The intellectual and technical concepts contained
   herein are proprietary to Neurasoft Consulting cc
   and its suppliers and are protected by trade secret or copyright law.
   Dissemination of this information or reproduction of this material
   is strictly forbidden unless prior written permission is obtained
   from Neurasoft Consulting cc.
   */
  angular.module('fusion-constants', []).constant('fusionVersion', '1.24').value('WebApiBaseUrl', '').value('InventoryDecimalPlaces', {
    Quantity: 2,
    SellingPrice: 2,
    CostPrice: 2
  }).value('InventoryEntryOptions', {
    CaptureProjectPerLine: false
  }).constant('ViewPortData', {
    MaxSmallViewPortSize: 767,
    isSmallViewPort: false
  }).constant('ApplicationUserRoles', {
    all: '*',
    admin: 'fusion-admin',
    agent: 'fusion-agent',
    purchaseOrders: 'fusion-purchase-orders',
    purchaseOrderAuthorisation: 'fusion-purchase-order-auth',
    viewCompletedPurchaseOrderAuthorisation: 'fusion-purchase-order-history',
    inventoryEnquiry: 'fusion-inventory-enquiry',
    customerEnquiry: 'fusion-customer-enquiry',
    purchaseOrderEdit: 'fusion-purchase-order-edit'
  }).constant('ApplicationState', {
    home: 'home',
    login: 'login',
    loginSessions: 'login-sessions',
    tools: 'tools',
    selectBranch: 'select-branch',
    viewPurchaseOrderList: 'viewPurchaseOrderList',
    viewPurchaseOrder: 'viewPurchaseOrder',
    editPurchaseOrder: 'editPurchaseOrder',
    listPurchaseOrders: 'listPurchaseOrders',
    authorisePurchaseOrders: 'authorisePurchaseOrders',
    completedAuthorisePurchaseOrders: 'completedAuthorisePurchaseOrders',
    authorisePurchaseOrder: 'authorisePurchaseOrder',
    viewPurchaseOrderAuthorisations: 'viewPurchaseOrderAuthorisations',
    agentAdmin: 'agentAdmin',
    customerEnquiry: 'customer-enquiry',
    suppliers: 'suppliers',
    inventoryItemEnquiry: 'inventory-item-enquiry',
    incidentTypeAmountLimits: 'incident-type-amount-limits'
  }).constant('ApplicationNotifications', {
    SetErrorMessage: 'SetErrorMessage',
    SetResponseStatus: 'SetResponseStatus',
    setFusionData: 'setFusionData',
    branchSelected: 'branchSelected',
    SuccessfulLogin: 'SuccessfulLogin',
    LoginComplete: 'LoginComplete',
    CurrentUserLoadedFromLocalStore: 'CurrentUserLoadedFromLocalStore',
    CurrentUserSet: 'CurrentUserSet',
    FailedLogin: 'FailedLogin',
    Logout: 'Logout',
    ShowMainMenu: 'ShowMainMenu',
    HideMainMenu: 'HideMainMenu',
    AfterApplicationConfigured: 'AfterApplicationConfigured',
    loginSuccess: 'auth-login-success',
    loginFailed: 'auth-login-failed',
    logoutSuccess: 'auth-logout-success',
    sessionTimeout: 'auth-session-timeout',
    notAuthenticated: 'auth-not-authenticated',
    notAuthorized: 'auth-not-authorized'
  }).constant('AgentProfileKeyNames', {
    PurchaseOrderGeneralLedgerAccounts: 'purchase-order-general-ledger-accounts'
  }).constant('DefaultTransactionDateOptions', {
    changeYear: true,
    changeMonth: true,
    yearRange: '-2:+2',
    dateFormat: 'dd/mm/yy'
  }).constant('DefaultGeneralDateOptions', {
    changeYear: true,
    changeMonth: true,
    dateFormat: 'dd/mm/yy'
  }).constant('OrderLineType', {
    Inventory: 'IC',
    GeneralLedger: 'GL'
  }).constant('PurchaseOrderApprovalStatusClass', function(purchaseOrderApprovalStatusId) {
    var approved, purchaseOrderApprovalStatusClass, rejected, waiting;
    waiting = 1;
    approved = 2;
    rejected = 3;
    purchaseOrderApprovalStatusClass = [];
    purchaseOrderApprovalStatusClass[waiting] = 'fusion-purchase-order-waiting';
    purchaseOrderApprovalStatusClass[approved] = 'fusion-purchase-order-approved';
    purchaseOrderApprovalStatusClass[rejected] = 'fusion-purchase-order-rejected';
    return purchaseOrderApprovalStatusClass[purchaseOrderApprovalStatusId];
  });

}).call(this);
