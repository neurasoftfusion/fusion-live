// Generated by CoffeeScript 1.11.1
(function() {
  'use strict';

  /*
  
   Neurasoft Consulting CONFIDENTIAL
   __________________
  
    Copyright (C) Neurasoft Consulting cc
    All Rights Reserved.
  
   NOTICE:  All information contained herein is, and remains
   the property of Neurasoft Consulting cc and its suppliers,
   if any.  The intellectual and technical concepts contained
   herein are proprietary to Neurasoft Consulting cc
   and its suppliers and are protected by trade secret or copyright law.
   Dissemination of this information or reproduction of this material
   is strictly forbidden unless prior written permission is obtained
   from Neurasoft Consulting cc.
   */
  var EditPurchaseOrderControllerDx, PurchaseOrderLinesGridSettings, addSamplePurchaseOrderData, addSamplePurchaseOrderHeaderData, injectDependencies, sampleAdd2ndPurchaseOrderLine, sampleAddPurchaseOrderLine;

  injectDependencies = ['$scope', '$stateParams', 'PurchaseOrder', 'PurchaseOrderFactory', 'AddressTabConfig', 'FusionResponseService', 'NavigationService', 'PurchaseOrdersWebApi', 'PdfViewerService', 'NotificationService', 'SendEmailPrompt', 'AnalyticsService', 'GeneralLedgerBudgets', 'InventoryEntryOptions', 'CompanyDefaultsService', 'MessageDialog', 'ApplicationState', 'AuthenticationService', 'ApplicationUserRoles'];

  EditPurchaseOrderControllerDx = function($scope, $stateParams, PurchaseOrder, PurchaseOrderFactory, AddressTabConfig, FusionResponseService, NavigationService, PurchaseOrdersWebApi, PdfViewerService, NotificationService, SendEmailPrompt, AnalyticsService, GeneralLedgerBudgets, InventoryEntryOptions, CompanyDefaultsService, MessageDialog, ApplicationState, AuthenticationService, ApplicationUserRoles) {
    var init, initForExistingPurchaseOrder, initForNewPurchaseOrder, isNewPurchaseOrder, model;
    model = this;
    $scope.viewOnly = $stateParams.viewType !== 'edit';
    $scope.allowEdit = $stateParams.viewType === 'edit';
    $scope.budgetStatusClass = 'purchase-order-budget-status-green-text';
    isNewPurchaseOrder = function() {
      return !(PurchaseOrder != null ? PurchaseOrder.header : void 0);
    };
    initForNewPurchaseOrder = function() {
      var ref, ref1;
      $scope.purchaseOrder = PurchaseOrderFactory.CreatePurchaseOrder();
      $scope.purchaseOrder.purchaseOrderAutoNumberEnabled = (ref = CompanyDefaultsService.fusionDataEntryOptions) != null ? ref.purchaseOrderAutoNumberEnabled : void 0;
      $scope.enterManualPurchaseOrderNumber = !((ref1 = CompanyDefaultsService.purchaseOrderDefaults) != null ? ref1.autoPurchaseOrderNumber : void 0) && !$scope.purchaseOrder.purchaseOrderAutoNumberEnabled;
      if ($scope.enterManualPurchaseOrderNumber) {
        $scope.purchaseOrder.purchaseOrderNumber = '';
        $scope.purchaseOrder.manualPurchaseOrderNumber = $scope.enterManualPurchaseOrderNumber;
      }
      return AnalyticsService.recordPageView("New Purchase Order");
    };
    initForExistingPurchaseOrder = function() {
      $scope.enterManualPurchaseOrderNumber = false;
      $scope.purchaseOrder = PurchaseOrderFactory.CopyFromApiObject(PurchaseOrder);
      if ($scope.viewOnly) {
        AnalyticsService.recordPageView("View Purchase Order");
      }
      if ($scope.allowEdit) {
        return AnalyticsService.recordPageView("Edit Purchase Order");
      }
    };
    if (isNewPurchaseOrder()) {
      initForNewPurchaseOrder();
    } else {
      initForExistingPurchaseOrder();
    }
    $scope.GetPurchaseOrderNumberDisplay = function() {
      if (!$scope.purchaseOrder.purchaseOrderNumber || $scope.purchaseOrder.purchaseOrderNumber === '') {
        return 'New order';
      }
      return $scope.purchaseOrder.purchaseOrderNumber;
    };
    $scope.onSupplierAccountSelected = function(supplier) {
      $scope.purchaseOrder.supplier = supplier;
      return $scope.SetDisablePlacePurchaseOrderButtonState();
    };
    $scope.addressTab = AddressTabConfig.GetSettings($scope);
    $scope.generalLedgerLineSettings = {};
    $scope.generalLedgerLineSettings.onAddGeneralLedgerLine = function(generalLedgerLine) {
      generalLedgerLine.accountBudgetData = generalLedgerLine.SelectedGeneralLedgerAccount.accountBudgetData;
      PurchaseOrderFactory.AddLineToPurchaseOrder($scope.purchaseOrder, generalLedgerLine);
      return $scope.SetDisablePlacePurchaseOrderButtonState();
    };
    $scope.generalLedgerLineSettings.onUpdateGeneralLedgerLine = function(generalLedgerLine) {
      generalLedgerLine.accountBudgetData = generalLedgerLine.SelectedGeneralLedgerAccount.accountBudgetData;
      return $scope.purchaseOrder.updateTotals();
    };
    $scope.addGeneralLedgerLine = function() {
      return $scope.generalLedgerLineSettings.newGeneralLedgerLine($scope.purchaseOrder);
    };
    $scope.editGeneralLedgerLine = function(generalLedgerLine) {
      return $scope.generalLedgerLineSettings.editGeneralLedgerLine(generalLedgerLine);
    };
    model.SetInventoryItem = function(inventoryItem) {
      console.log("inventory Item");
      return console.log(inventoryItem);
    };
    $scope.addInventoryLine = function() {
      return console.log("addInventoryLine");
    };
    $scope.selectedFiles = [];
    $scope.purchaseOrderLinesGridSettings = PurchaseOrderLinesGridSettings;
    $scope.EditOrderLine = function(purchaseOrderLine) {
      if (purchaseOrderLine.isInventoryItemLineType()) {

      } else {
        return $scope.editGeneralLedgerLine(purchaseOrderLine);
      }
    };
    $scope.DeleteOrderLine = function(purchaseOrderLine) {
      if (purchaseOrderLine.isNewPurchaseOrderLine()) {
        PurchaseOrderFactory.RemoveLineFromPurchaseOrder($scope.purchaseOrder, purchaseOrderLine);
      } else {
        purchaseOrderLine.isDeleted = true;
      }
      $scope.purchaseOrder.updateTotals();
      return $scope.SetDisablePlacePurchaseOrderButtonState();
    };
    $scope.UnDeleteOrderLine = function(purchaseOrderLine) {
      purchaseOrderLine.isDeleted = false;
      $scope.purchaseOrder.updateTotals();
      return $scope.SetDisablePlacePurchaseOrderButtonState();
    };
    $scope.DoNotAllowPlacePurchaseOrder = function() {
      return !$scope.purchaseOrder.hasSupplier() || $scope.purchaseOrder.purchaseOrderLines.length === 0;
    };
    $scope.SetDisablePlacePurchaseOrderButtonState = function() {
      return $scope.DisablePlacePurchaseOrderButton = $scope.viewOnly || !$scope.purchaseOrder.hasSupplier() || $scope.purchaseOrder.purchaseOrderLines.length === 0 || !$scope.allowPurchaseOrderEdit();
    };
    $scope.allowPurchaseOrderEdit = function() {
      return isNewPurchaseOrder() || AuthenticationService.isCurrentUserInRole(ApplicationUserRoles.purchaseOrderEdit);
    };
    $scope.PlacePurchaseOrder = function() {
      if ($scope.purchaseOrder.manualPurchaseOrderNumber && _.isEmptyString($scope.purchaseOrder.purchaseOrderNumber)) {
        MessageDialog.ShowErrorMessage('Purchase Order', 'Please enter a purchase order number.');
        return;
      }
      if (!InventoryEntryOptions.CaptureProjectPerLine && InventoryEntryOptions.ForceProject && !$scope.purchaseOrder.SelectedProject) {
        MessageDialog.ShowErrorMessage('Purchase Order', 'Please select a project.');
        return;
      }
      AnalyticsService.recordEvent("Save Purchase Order");
      PurchaseOrderFactory.UnlinkLinesFromPurchaseOrder($scope.purchaseOrder);
      return PurchaseOrdersWebApi.postSavePurchaseOrder($scope.purchaseOrder).then(function() {
        if (FusionResponseService.HasErrors()) {
          PurchaseOrderFactory.LinkLinesToPurchaseOrder($scope.purchaseOrder);
          return;
        }
        NotificationService.toastSuccess('Purchase Order saved');
        return $scope.GoBack();
      });
    };
    $scope.BudgetMessage = function(purchaseOrderLine) {
      return GeneralLedgerBudgets.GetCombinedBudgetMessage(purchaseOrderLine.accountBudgetData);
    };
    $scope.BudgetMessageClass = function(purchaseOrderLine) {
      return GeneralLedgerBudgets.GetBudgetStatusClass(purchaseOrderLine.accountBudgetData);
    };
    $scope.EnforceBudgets = function(purchaseOrderLine) {
      return GeneralLedgerBudgets.EnforceBudgets(purchaseOrderLine.accountBudgetData);
    };
    $scope.GoBack = function() {
      var params;
      if ($stateParams.returnTo && $stateParams.returnTo !== '') {
        params = null;
        if ($stateParams.returnTo === ApplicationState.authorisePurchaseOrder) {
          params = $stateParams.returnToId ? {
            incidentId: $stateParams.returnToId
          } : null;
        }
        return NavigationService.goTo($stateParams.returnTo, params);
      } else {
        return NavigationService.goListPurchaseOrders();
      }
    };
    $scope.printPurchaseOrder = function() {
      AnalyticsService.recordEvent("Print Purchase Order");
      return PurchaseOrdersWebApi.getPurchaseOrderPdf($scope.purchaseOrder.purchaseOrderId).then(function(data) {
        return PdfViewerService.OpenPdf(data.fusion.data.fileName);
      });
    };
    $scope.emailPurchaseOrder = function() {
      var emailOptions;
      AnalyticsService.recordEvent("Email Purchase Order");
      emailOptions = {
        emailTo: {
          name: $scope.purchaseOrder.supplier.accountContactPerson,
          emailAddress: $scope.purchaseOrder.supplier.accountEmailAddress
        }
      };
      return SendEmailPrompt.ShowSendEmailDialog('Send Purchase Order to Supplier', emailOptions, function() {
        emailOptions.purchaseOrderId = $scope.purchaseOrder.purchaseOrderId;
        return PurchaseOrdersWebApi.emailPurchaseOrderPdf(emailOptions).then(function(data) {
          if (!FusionResponseService.HasErrors()) {
            NotificationService.toastSuccess('Email sent');
          }
          if (FusionResponseService.HasErrors()) {
            return NotificationService.toastError('Email send failed');
          }
        });
      });
    };
    init = function() {
      $scope.allowProjectEntry = !InventoryEntryOptions.CaptureProjectPerLine;
      $scope.allowIncidentTypeEntry = CompanyDefaultsService.fusionDataEntryOptions.allowIncidentTypeEntry;
      return $scope.SetDisablePlacePurchaseOrderButtonState();
    };
    init();
    return null;
  };

  EditPurchaseOrderControllerDx.$inject = injectDependencies;

  PurchaseOrderLinesGridSettings = {
    bindingOptions: {
      dataSource: 'purchaseOrder.purchaseOrderLines'
    },
    filterRow: {
      visible: false
    },
    headerFilter: {
      visible: false
    },
    searchPanel: {
      visible: false
    },
    paging: {
      enabled: true
    },
    pager: {
      showPageSizeSelector: true,
      allowedPageSizes: [20, 50, 100],
      showNavigationButtons: true
    },
    selection: {
      mode: 'none'
    },
    showBorders: false,
    rowAlternationEnabled: true,
    allowColumnResizing: true,
    columnAutoWidth: true,
    hoverStateEnabled: true,
    wordWrapEnabled: true,
    showColumnHeaders: false,
    columns: [
      {
        cellTemplate: 'purchaseOrderLineCellTemplate',
        caption: 'Description'
      }
    ]
  };

  addSamplePurchaseOrderHeaderData = function($scope) {
    $scope.purchaseOrder.branchId;
    $scope.purchaseOrder.supplier.currencyId = 1;
    $scope.purchaseOrder.supplier.supplierId = 15;
    $scope.purchaseOrder.supplier.supplierAccountCode = 'GEE001';
    $scope.purchaseOrder.supplier.supplierName = 'Geek Zone';
    $scope.purchaseOrder.supplier.streetAddressLine1 = '87 The Grove Business Park';
    $scope.purchaseOrder.supplier.streetAddressLine2 = 'Garfield Road';
    $scope.purchaseOrder.supplier.streetAddressLine3 = 'Buccleuch';
    $scope.purchaseOrder.supplier.streetAddressLine4 = 's4';
    $scope.purchaseOrder.supplier.streetAddressLine5 = 's5';
    $scope.purchaseOrder.supplier.streetAddressLinePostalCode = 'spc';
    $scope.purchaseOrder.supplier.postalAddressLine1 = 'P O Box 143';
    $scope.purchaseOrder.supplier.postalAddressLine2 = 'Buccleuch';
    $scope.purchaseOrder.supplier.postalAddressLine3 = 'p3';
    $scope.purchaseOrder.supplier.postalAddressLine4 = 'p4';
    $scope.purchaseOrder.supplier.postalAddressLine5 = 'p5';
    return $scope.purchaseOrder.supplier.postalAddressLinePostalCode = 'ppc';
  };

  sampleAddPurchaseOrderLine = function($scope, PurchaseOrderFactory) {
    var line;
    line = PurchaseOrderFactory.CreateGeneralLedgerLine();
    line.SelectedGeneralLedgerAccount = {
      accountId: 3,
      accountCode: "2100",
      accountDescription: "Purchases",
      purchaseTaxTypeId: 3
    };
    line.accountBudgetData = {
      ignore: false,
      period: 62,
      periodActual: 5500,
      periodBudget: 51243.5,
      periodEndDate: "2016-04-30T00:00:00.0000000+02:00",
      unprocessedPOValue: 4873.68,
      useAnnualBudget: false,
      ytdActual: 2500,
      ytdBudget: 102487
    };
    line.SelectedTaxType = {
      code: '3',
      description: 'Input Tax',
      taxRate: 14,
      taxTypeId: 3
    };
    line.Description = 'Consulting services for management reports.';
    line.OrderQuantity = 10;
    line.UnitPriceExVAT = 1000;
    line.LineDiscountPercentage = 10;
    line.LineNote = 'This is a line note\r that has multiple lines';
    PurchaseOrderFactory.AddLineToPurchaseOrder($scope.purchaseOrder, line);
    line.SetUnitPriceInVATFromUnitPriceExVAT(line);
    return line;
  };

  sampleAdd2ndPurchaseOrderLine = function($scope, PurchaseOrderFactory) {
    var line;
    line = sampleAddPurchaseOrderLine($scope, PurchaseOrderFactory);
    line.Description = 'Travel charges';
    line.OrderQuantity = 1;
    line.UnitPriceExVAT = 180;
    line.isDeleted = true;
    return line.accountBudgetData.periodActual = 55000;
  };

  addSamplePurchaseOrderData = function($scope, PurchaseOrderFactory) {
    addSamplePurchaseOrderHeaderData($scope);
    $scope.onSupplierAccountSelected($scope.purchaseOrder.supplier);
    sampleAddPurchaseOrderLine($scope, PurchaseOrderFactory);
    sampleAdd2ndPurchaseOrderLine($scope, PurchaseOrderFactory);
    return $scope.purchaseOrder.updateTotals();
  };

  angular.module('fusion-purchase-orders').controller('EditPurchaseOrderControllerDx', EditPurchaseOrderControllerDx);

}).call(this);
